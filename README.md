# Personal Configurations

## Prerequisites

- [nala](https://gitlab.com/volian/nala) - a front-end for apt-get
- [zsh](https://www.zsh.org/) - a linux shell
- [ohmyzsh](https://ohmyz.sh/) - a framework for managing zsh configurations

## Programs

- [Powerlevel10k](https://github.com/romkatv/powerlevel10k) - a theme for zsh
- [kitty](https://sw.kovidgoyal.net/kitty/) - a GPU based terminal emulator
- [nvm](https://github.com/nvm-sh/nvm) - node version manager
- [yarn](https://yarnpkg.com/) - a package manager for node
- [php](https://www.php.net/) - a server-side scripting language
- [Neovim](https://neovim.io/) - a hyperextensible Vim-based text editor
- [VSCode](https://code.visualstudio.com/) - a advance code editor
- [Jetbrains Toolbox](https://www.jetbrains.com/toolbox/) - a collection of tools for Jetbrains
- [PHPStorm](https://www.jetbrains.com/phpstorm/) - a PHP IDE from Jetbrains
- [Docker Desktop](https://www.docker.com/products/docker-desktop) - a desktop environment for docker
- [Mullvad VPN](https://www.mullvad.net/) - a VPN client

## List of custom Neovim configs

- [alpha2phi/neovim-for-minimalist](https://github.com/alpha2phi/neovim-for-minimalist)
- [craftzdog/dotfiles-public](https://github.com/craftzdog/dotfiles-public)
