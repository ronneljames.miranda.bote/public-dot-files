#!/usr/bin/env zsh

# Install nvm binaries using installer from nvm
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash

source "$HOME/.bashrc"
source "$HOME/.profile"
source "$HOME/.zshrc"

# Install versions of nodejs from nvm
nvm install node
nvm install lts/gallium
nvm install lts/fermium
nvm install lts/erbium
