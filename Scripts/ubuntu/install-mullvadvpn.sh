#!/usr/bin/env bash

deb=/tmp/mullvad-vpn.deb
installer=nala

wget -O $deb https://mullvad.net/download/app/deb/latest -q --show-progress

# Check if (nala)[https://gitlab.com/volian/nala] exists
if ! command -v nala > /dev/null 2>&1; then
  installer=apt
fi

sudo $installer install $deb
