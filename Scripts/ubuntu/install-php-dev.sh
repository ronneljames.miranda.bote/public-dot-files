#!/usr/bin/env bash

### Install PHP
installer=nala

# Check if (nala)[https://gitlab.com/volian/nala] exists
if ! command -v nala > /dev/null 2>&1; then
  installer=apt
fi

sudo add-apt-repository ppa:ondrej/php -y
sudo $installer update
sudo $installer install php8.1-cli php7.4-cli \
  php8.1-zip php7.4-zip \
  php8.1-curl php7.4-curl \
  php8.1-mbstring php7.4-mbstring \
  php8.1-xml php7.4-xml

### Install composer

EXPECTED_CHECKSUM="$(php -r 'copy("https://composer.github.io/installer.sig", "php://stdout");')"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_CHECKSUM="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"

if [ "$EXPECTED_CHECKSUM" != "$ACTUAL_CHECKSUM" ]
then
    >&2 echo 'ERROR: Invalid installer checksum'
    rm composer-setup.php
    exit 1
fi

php composer-setup.php --filename=composer1 --install-dir=$HOME/.local/bin --1
php composer-setup.php --filename=composer2 --install-dir=$HOME/.local/bin --2

rm composer-setup.php

# Set update-alternatives for composer command
sudo update-alternatives --install /usr/local/bin/composer composer $HOME/.local/bin/composer2 40
sudo update-alternatives --install /usr/local/bin/composer composer $HOME/.local/bin/composer1 20
